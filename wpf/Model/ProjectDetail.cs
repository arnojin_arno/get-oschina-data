﻿using MySql.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;


namespace GetOSChinaData.Model
{
    /// <summary>
    /// 软件项目/悬赏任务 详情
    /// </summary>
    [Table("project_detail")]
    public partial class ProjectDetail
    {
        public ProjectDetail()
        {

        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("project_detail_id")]
        public int projectDetailId { get; set; }

        [StringLength(500)]
        [Key]
        [Column("name", Order = 0)]
        public string name { get; set; }

        [StringLength(100)]
        [Key]
        [Column("publish_time", Order = 1)]
        public string publishTime { get; set; }

        [StringLength(200)]
        [Key]
        [Column("user_account_nickname", Order = 2)]
        public string userAccountNickname { get; set; }

        [Column("id")]
        public int? id { get; set; }

        [StringLength(200)]
        [Column("project_no")]
        public string projectNo { get; set; }

        [Column("user_id")]
        public int? userId { get; set; }

        [Column("user_account_id")]
        public int userAccountId { get; set; }

        [StringLength(1073741823)]
        [Column("image_path")]
        public string imagePath { get; set; }

        [Column("budget_min")]
        public int budgetMin { get; set; }

        [Column("budget_max")]
        public int budgetMax { get; set; }

        [Column("cycle")]
        public int cycle { get; set; }

        [StringLength(1073741823)]
        [Column("cycle_name")]
        public string cycleName { get; set; }

        [Column("pay_type")]
        public int? payType { get; set; }

        [Column("is_tendency_district")]
        public int? isTendencyDistrict { get; set; }

        [StringLength(1073741823)]
        [Column("tendency_province")]
        public string tendencyProvince { get; set; }

        [StringLength(1073741823)]
        [Column("tendency_city")]
        public string tendencyCity { get; set; }

        [StringLength(1073741823)]
        [Column("tendency_type")]
        public string tendencyType { get; set; }

        [Column("status")]
        public int status { get; set; }

        [StringLength(1073741823)]
        [Column("status_last_time")]
        public string statusLastTime { get; set; }

        [Column("sub_status")]
        public int? subStatus { get; set; }

        [Column("deposit_hosting_status")]
        public int? depositHostingStatus { get; set; }

        [Column("deposit_money")]
        public int? depositMoney { get; set; }

        [StringLength(1073741823)]
        [Column("service_fee")]
        public string serviceFee { get; set; }

        [StringLength(1073741823)]
        [Column("skills")]
        public string skills { get; set; }

        [Column("service_fee_pay_status")]
        public int? serviceFeePayStatus { get; set; }

        [StringLength(1073741823)]
        [Column("service_fee_pay_time")]
        public string serviceFeePayTime { get; set; }

        [Column("attachment_visible")]
        public int? attachmentVisible { get; set; }

        [Column("attachment_count")]
        public int? attachmentCount { get; set; }

        [Column("apply_count")]
        public int applyCount { get; set; }

        [Column("view_count")]
        public int viewCount { get; set; }

        [Column("visible")]
        public int? visible { get; set; }

        [Column("handle_status")]
        public int? handleStatus { get; set; }

        [Column("publish_status")]
        public int? publishStatus { get; set; }

        [Column("audit_status")]
        public int? auditStatus { get; set; }

        [StringLength(1073741823)]
        [Column("audit_reason")]
        public string auditReason { get; set; }

        [StringLength(1073741823)]
        [Column("audit_time")]
        public string auditTime { get; set; }

        [Column("resident_require")]
        public int? residentRequire { get; set; }

        [StringLength(1073741823)]
        [Column("budget_min_by_yuan")]
        public string budgetMinByYuan { get; set; }

        [StringLength(1073741823)]
        [Column("budget_max_by_yuan")]
        public string budgetMaxByYuan { get; set; }

        [StringLength(1073741823)]
        [Column("application")]
        public string application { get; set; }

        [Column("top")]
        public int top { get; set; }


        [Column("type")]
        public int type { get; set; }

        [StringLength(1073741823)]
        [Column("user_account_icon_path")]
        public string userAccountIconPath { get; set; }

        [Column("can_apply_by_pulish_time")]
        public bool canApplyByPulishTime { get; set; }

        [StringLength(1073741823)]
        [Column("service_fee_by_yuan")]
        public string serviceFeeByYuan { get; set; }
    }
}
