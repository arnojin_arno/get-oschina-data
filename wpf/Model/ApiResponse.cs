﻿using System.Collections.Generic;

namespace GetOSChinaData.Model
{
    /// <summary>
    ///  api 返回结果
    /// </summary>
    public class ApiResponse
    {
        public int code { get; set; }
        public bool success { get; set; }
        public string timestamp { get; set; }

        /// <summary>
        /// 软件项目 列表
        /// </summary>
        public List<ProjectDetail> project_list { get; set; }

        /// <summary>
        /// 悬赏任务 列表
        /// </summary>
        public List<ProjectDetail> reward_list { get; set; }

        /// <summary>
        /// 最新成交 列表
        /// </summary>
        public List<ProjectConfirmed> confirmed_list { get; set; }
    }
}
