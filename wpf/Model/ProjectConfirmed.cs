﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GetOSChinaData.Model
{
    /// <summary>
    /// 最新成交 
    /// </summary>
    [Table("project_confirmed")]
    public class ProjectConfirmed
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("project_confirmed_id")]
        public int projectConfirmedId { get; set; }

        [Key]
        [StringLength(200)]
        [Column("contractor_account_nickname", Order = 0)]
        public string contractorAccountNickname { get; set; }

        [Key]
        [Column("money", Order = 1)]
        public int money { get; set; }

        [Key]
        [StringLength(200)]
        [Column("project_name", Order = 2)]
        public string projectName { get; set; }

        [StringLength(200)]
        [Column("money_by_yuan")]
        public string moneyByYuan { get; set; }

    }
}
