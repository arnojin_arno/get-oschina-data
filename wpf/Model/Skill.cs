﻿using MySql.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;


namespace GetOSChinaData.Model
{
    /// <summary>
    /// 技能要求
    /// </summary>
    [Table("skill")]
    public partial class Skill
    {
        [Key]
        [Column("skill_id")]
        public int skillId { get; set; }

        [StringLength(500)]
        [Column("value")]
        public string value { get; set; }
    }
}
