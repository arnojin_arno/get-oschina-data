﻿using MySql.Data.Entity;
using System.Data.Entity;

namespace GetOSChinaData.Model
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class ModelOSChina : DbContext
    {
        public ModelOSChina() : base("name=ModelOSChina")
        {
        }

        /// <summary>
        /// 最新成交 列表
        /// </summary>
        public virtual DbSet<ProjectConfirmed> ProjectConfirmeds { get; set; }

        /// <summary>
        /// 软件项目/悬赏任务 列表
        /// </summary>
        public virtual DbSet<ProjectDetail> ProjectDetails { get; set; }

        /// <summary>
        /// 技能要求 列表
        /// </summary>
        public virtual DbSet<Skill> Skills { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
