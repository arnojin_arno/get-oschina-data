# 显示 PowerShell 版本
$PSVersionTable

# 修改编码页为 UTF-8，避免 java 输出日志乱码
chcp 65001

# 获取 ps1 脚本所在路径
$PowerShellScript_Path = Split-Path -Parent $MyInvocation.MyCommand.Definition

# 切换执行路径
Set-Location $PowerShellScript_Path

# 显示 Java 版本
java -version

# 运行
java -jar .\build\libs\api-1.0.0.jar
