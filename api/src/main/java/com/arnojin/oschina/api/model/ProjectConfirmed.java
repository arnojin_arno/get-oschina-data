package com.arnojin.oschina.api.model;

import javax.persistence.*;

/**
 * 最新成交
 *
 * @author arno
 * @date 2021/01/26 22:02
 */
@Entity
@Table(name = "project_confirmed")
@IdClass(ProjectConfirmedPk.class)
public class ProjectConfirmed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, insertable = false, updatable = false, columnDefinition = "bigint(20) not null UNIQUE key auto_increment")
    public Integer projectConfirmedId;

    @Id
    public Integer money;

    @Id
    public String contractorAccountNickname;

    @Id
    public String projectName;

    public String moneyByYuan;
}