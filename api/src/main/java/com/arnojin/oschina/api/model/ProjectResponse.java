package com.arnojin.oschina.api.model;

import com.arnojin.oschina.api.model.Project;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 软件项目/悬赏任务 返回结果
 *
 * @author arno
 * @date 2021/01/25 18:20
 */
public class ProjectResponse {

    public ProjectResponse() {
        this.success = true;
        this.code = 200;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        this.timestamp = formatter.format(date);
    }

    public boolean success;
    public int code;
    public String timestamp;
    public String message;
    public Project data;
}
