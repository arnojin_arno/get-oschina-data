package com.arnojin.oschina.api.repository;

import com.arnojin.oschina.api.model.ProjectConfirmed;
import com.arnojin.oschina.api.model.ProjectConfirmedPk;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ProjectConfirmed Repository
 *
 * @author arno
 * @date 2021/02/15 14:08
 */
public interface ProjectConfirmedRepository extends JpaRepository<ProjectConfirmed, ProjectConfirmedPk> {
}
