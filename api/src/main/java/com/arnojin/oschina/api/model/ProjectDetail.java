package com.arnojin.oschina.api.model;

import javax.persistence.*;
import java.util.List;

/**
 * 软件项目/悬赏任务 明细
 *
 * @author arno
 * @date 2021/01/26 22:04
 */
@Entity
@Table(name = "project_detail")
@IdClass(ProjectDetailPk.class)
public class ProjectDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, insertable = false, updatable = false, columnDefinition = "bigint(20) not null UNIQUE key auto_increment")
    public Integer projectDetailId;

    @Id
    public String userAccountNickname;
    @Id
    public String name;
    @Id
    public String publishTime;

    @Transient
    public List<Skill> skillList;
    public String skills;

    @Column(nullable = true)
    public Integer id;
    public String projectNo;
    @Column(nullable = true)
    public Integer userId;
    @Column(nullable = true)
    public Integer userAccountId;
    public String imagePath;
    @Column(nullable = true)
    public Integer budgetMin;
    @Column(nullable = true)
    public Integer budgetMax;
    @Column(nullable = true)
    public Integer cycle;
    public String cycleName;
    @Column(nullable = true)
    public Integer payType;
    @Column(nullable = true)
    public Integer isTendencyDistrict;
    public String tendencyProvince;
    public String tendencyCity;
    public String tendencyType;
    @Column(nullable = true)
    public Integer status;
    @Column(nullable = true)
    public String statusLastTime;
    @Column(nullable = true)
    public Integer subStatus;
    @Column(nullable = true)
    public Integer depositHostingStatus;
    @Column(nullable = true)
    public Integer depositMoney;
    public String serviceFee;
    @Column(nullable = true)
    public Integer serviceFeePayStatus;
    public String serviceFeePayTime;
    @Column(nullable = true)
    public Integer attachmentVisible;
    @Column(nullable = true)
    public Integer attachmentCount;
    @Column(nullable = true)
    public Integer applyCount;
    @Column(nullable = true)
    public Integer viewCount;
    @Column(nullable = true)
    public Integer visible;
    @Column(nullable = true)
    public Integer handleStatus;
    @Column(nullable = true)
    public Integer publishStatus;
    @Column(nullable = true)
    public Integer auditStatus;
    public String auditReason;
    public String auditTime;
    @Column(nullable = true)
    public Integer residentRequire;
    public String budgetMinByYuan;
    public String budgetMaxByYuan;
    public String application;
    @Column(nullable = true)
    public Integer top;
    @Column(nullable = true)
    public Integer type;
    public String userAccountIconPath;
    public Boolean canApplyByPulishTime;
    public String serviceFeeByYuan;
}