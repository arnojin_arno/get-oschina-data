package com.arnojin.oschina.api.model;

import lombok.Data;

import java.io.Serializable;

/**
 * ProjectDetail 的主键类
 *
 * @author arno
 * @date 2021/02/15 12:26
 */
@Data
public class ProjectDetailPk implements Serializable {
    public String userAccountNickname;
    public String name;
    public String publishTime;
}
