package com.arnojin.oschina.api.repository;

import com.arnojin.oschina.api.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Skill Repository
 *
 * @author arno
 * @date 2021/02/15 14:08
 */
@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {
}
