package com.arnojin.oschina.api.repository;

import com.arnojin.oschina.api.model.ProjectDetail;
import com.arnojin.oschina.api.model.ProjectDetailPk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ProjectDetail Repository
 *
 * @author arno
 * @date 2021/02/14 22:48
 */
@Repository
public interface ProjectDetailRepository extends JpaRepository<ProjectDetail, ProjectDetailPk> {
}
