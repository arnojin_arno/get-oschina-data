package com.arnojin.oschina.api;

import java.util.TimeZone;

import com.arnojin.oschina.api.util.SpringUtil;
import com.arnojin.oschina.api.web.ProjectController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 获取开源众包数据 API
 *
 * @author arno
 * @date 2021/01/25
 */
@SpringBootApplication
@EnableScheduling
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Shanghai"));

        ProjectController projectController = SpringUtil.getBean(ProjectController.class);
        projectController.start();
    }
}
