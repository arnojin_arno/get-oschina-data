package com.arnojin.oschina.api.job;

import com.arnojin.oschina.api.web.ProjectController;
import com.arnojin.oschina.api.util.SpringUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 通过定时任务获取 OsChina 数据
 *
 * @author arno
 * @date 2021/01/27 20:50
 */
public class Job1 implements Job {

    static final Logger logger = LoggerFactory.getLogger(Job1.class);

    @Override
    public void execute(JobExecutionContext context) {
        ProjectController apiService = SpringUtil.getBean(ProjectController.class);
        apiService.getDataFromOsChina();
        logger.info(
                String.format("%s is running."
                        , context.getJobDetail().getKey().getName()
                )
        );
    }

}