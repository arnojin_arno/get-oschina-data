package com.arnojin.oschina.api.model;

import java.util.List;

/**
 * 最新成交 返回结果
 *
 * @author arno
 * @date 2021/01/26 22:01
 */
public class ProjectConfirmedResponse {
    public int code;
    public String message;
    public List<ProjectConfirmed> data;
    public boolean success;
}