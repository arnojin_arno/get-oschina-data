package com.arnojin.oschina.api.model;

import javax.persistence.*;

/**
 * 技能
 *
 * @author arno
 * @date 2021/01/26 22:05
 */
@Entity
@Table(name = "skill", indexes = {@Index(columnList = "value")})
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, insertable = false, updatable = false, columnDefinition = "bigint(20) not null UNIQUE key auto_increment")
    public Integer skillId;

    public String value;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Skill) {
            return this.value.equals(((Skill) obj).value);
        }

        return false;
    }
}
