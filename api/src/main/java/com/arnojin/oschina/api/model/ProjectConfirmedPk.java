package com.arnojin.oschina.api.model;

import lombok.Data;

import java.io.Serializable;

/**
 * ProjectConfirmed 主键
 *
 * @author arno
 * @date 2021/02/16 11:24
 */
@Data
public class ProjectConfirmedPk implements Serializable {
    public int money;
    public String contractorAccountNickname;
    public String projectName;
}