package com.arnojin.oschina.api.model;

import java.util.List;

/**
 * 软件项目/悬赏任务
 *
 * @author arno
 * @date 2021/01/25 18:23
 */
public class Project {
    public int offset;
    public int limit;
    public String currentTime;
    public List<ProjectDetail> data;
    public int totalCount;
    public int pageSize;
    public int currentPage;
    public int totalPage;
}