# 获取开源众包数据

#### 介绍
1. 桌面软件为WPF程序，可以在Win XP/Win 7/Win 10上运行，工程文件 [wpf\GetOSChinaData.sln](wpf/GetOSChinaData.sln) 。
1. API软件为SpringBoot程序，可以在Win/Linux上运行，工程文件 [api\build.gradle](api/build.gradle) 。
1. 前期功能是获取 [开源众包](https://zb.oschina.net) 上 软件项目、悬赏任务、最新成交 的数据，在界面上呈现，并可以写入数据库。 
1. 后续增加微信消息推送，将信息通过微信公众号推送给关注了公众号的人（待实现）。

#### 开发环境
* [配置 Visual Studio 2019 的 MySQL 5.7 + EntityFramework 6.0 开发环境用于 WinXP 的 WinForm/WPF 程序开发](https://blog.arnojin.com/?p=550)
* Microsoft Visual Studio Community 2019
* MySQL Community 5.7.32
* IntelliJ IDEA 2020.2.3 (Ultimate Edition)
    * Plugins
        * Alibaba Java Coding Guidelines plugin support
        * IntelliJ Lombok plugin
        * JRebel and XRebel for InteLLiJ
        * Free MyBatis plugin

#### wpf 库版本
* .NET Framework 4.0
* EntityFramework 6.0.0.0
* MySql.Data 6.9.12.0
* MySql.Data.Entity.EF6 6.9.12.0
* Newtonsoft.Json 12.0.0.0
* log4net 2.0.12.0

#### api 库版本
* ch.qos.logback:logback-classic:1.2.3
* com.alibaba:druid-spring-boot-starter:1.2.4
* com.google.code.gson:gson:2.8.6
* com.google.guava:guava:30.1-jre
* com.squareup.okhttp3:logging-interceptor:3.14.9
* com.squareup.okhttp3:okhttp:3.14.9
* mysql:mysql-connector-java:8.0.22
* org.projectlombok:lombok:1.18.16
* org.springframework.boot:spring-boot-starter-data-jpa:2.4.2
* org.springframework.boot:spring-boot-starter-quartz:2.4.2
* org.springframework.boot:spring-boot-starter-web:2.4.2
* org.springframework.boot:spring-boot-starter:2.4.2

#### 数据准备
```text
在MySQL数据库中创建 os_china 数据库。
```
```sql
-- 创建 os_china 数据库
CREATE DATABASE `os_china` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- 赋予 root 外部访问权限
use mysql;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456' WITH GRANT OPTION;
flush privileges;
```
```text
在 App.config, application.properties 中配置数据库连接字符串用户、密码。
本机开发，需要在 C:\Windows\System32\drivers\etc\hosts 中设置 127.0.0.1 arno_mysql
```
[App.config](wpf/App.config)
```xml
  <connectionStrings>
    <add name="ModelOSChina" connectionString="server=arno_mysql;user id=root;Password=123456;database=os_china;allowuservariables=True;Character Set=utf8mb4" providerName="MySql.Data.MySqlClient" />
  </connectionStrings>
  <appSettings>
    <add key="ServerUrl" value="http://localhost:8080" />
  </appSettings>
```
[application.properties](api/src/main/resources/application.properties)
```groovy
spring.datasource.url=jdbc:mysql://arno_mysql:3306/os_china?useUnicode=true&characterEncoding=utf8&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&useSSL=false
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.username=root
spring.datasource.password=123456
```

#### 运行

```
在 Windows 7/10 下，可以运行以下PowerShell脚本来启动 api 服务
```
[api/run.ps1 脚本](api/run.ps1)

#### 程序界面
* 软件项目
![软件项目](file/001.png)
* 悬赏任务
![悬赏任务](file/002.png)
* 最新成交
![最新成交](file/003.png)
* 模糊查询
![模糊查询](file/004.png)
* 启动 api 服务
![启动 api 服务](file/005.png)
